<?php

use Rbnb\System\Authentification\Encryption;

return [
	'site_full_name' => 'rbnb.ru',
	'site_name' => 'www.rbnb.ru',
	'site_version' => '1.0',

	// Database
	'db_host' => '127.0.0.1',
	'db_user' => 'root',
	'db_pass' => '',
	'db_name' => 'rbnb',

	//Hash
	'salt' => 'SEL',
	'pepper' => 'POIVRE',
	'encryption_pattern' => [
		[Encryption::MD5 => 4],
		[Encryption::SHA512 => 6],
		[Encryption::SHA256 => 2]
	],

	//Profile
	'default_avatar' => 'img/users/default.png',
	'default_bio' => "Je n'ai pas de bio :/"
];
