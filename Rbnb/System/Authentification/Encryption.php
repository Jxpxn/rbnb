<?php

namespace Rbnb\System\Authentification;

class Encryption {
    public const MD5 = 'md5';
    public const SHA512 = 'sha512';
    public const SHA256 = 'sha256';

    private $salt = '';
    private $pepper = '';

    public function getSalt(): string { return $this->salt; }
    public function getPepper(): string { return $this->pepper; }

    public function __construct(string $salt = '', string $pepper = '') {
        $this->salt = $salt;
        $this->pepper = $pepper;
    }

    public function encrypt(string $arg, array $pattern, bool $sp = true): string {
        $output = $sp ? $this->salt . $arg . $this->pepper : $arg;
        if(count($pattern) > 0) {
            foreach($pattern as $p) {
                if(is_array($p) && count($p) > 0) {
                    foreach($p as $key => $value) {
                        for($i = 0; $i < $value; $i++) {
                            $output = \hash($key, $output);
                        }
                    }
                }
            }
        }
        return $output;
    }
}