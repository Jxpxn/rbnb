<?php

namespace Rbnb\System\Authentification;

use Rbnb\Rbnb;
use Rbnb\Database\Repository\RepositoryManager;

use Rbnb\Utils\ArrayUtils;

use Rbnb\Database\Model\User;
use Rbnb\System\Session\SessionUser;
use Rbnb\System\Session\Session;
use Rbnb\System\Session\Session_Dictionnary;

use Rbnb\Settings;

class Auth {
    private $encryption_instance = null;

    public function hashPassword(string $password): string {
        return $this->hashString(
            $password,
            Settings::instance()->get('encryption_pattern')
        );
    }

    public function hashSessionPassword(string $password) {
        return $this->hashString(
            $password,
            ArrayUtils::subArray( Settings::instance()->get('encryption_pattern'), 0, 0 )
        );
    }

    public function SessionPasswordToPassword(string $password) {
        return $this->hashString(
            $password,
            ArrayUtils::subArray( Settings::instance()->get('encryption_pattern'), 1 ),
            false
        );
    }

    private function hashString(string $string, array $pattern, bool $sp = true): string {
        return $this->encryption_instance->encrypt(
            $string,
            $pattern,
            $sp
        );
    }

    public function __construct() {
        $this->encryption_instance = new Encryption(
            Settings::instance()->get('salt'),
            Settings::instance()->get('pepper')
        );
    }

    public function login( string $email, string $password ): ?User {
        $auth = Rbnb::instance()->getAuth();

        $user_repo = RepositoryManager::instance()->getRepository('user');
        $user = $user_repo->getByEmail($email);

        if(!$user || hashPassword($password) != $user->password) {
            return null;
        }

        return $user;
    }

    public function loginFromSession( string $email, string $password ): ?User {
        $auth = Rbnb::instance()->getAuth();

        $user_repo = RepositoryManager::instance()->getRepository('user');
        $user = $user_repo->getByEmail($email);

        if(!$user) {
            return null;
        }
        $password_cleaned = $this->SessionPasswordToPassword($password);

        if($password_cleaned != $user->password) {
            return null;
        }

        return $user;
    }

    public function checkIfSessionIsCorrect($arg): array {
        $output = [
            'valide' => false,
            'user' => null
        ];

        if(!is_null($arg) && $arg instanceof SessionUser) {
            $credentials = $arg->toCredentials();

            $user = $this->loginFromSession(
                $credentials['email'],
                $credentials['password']
            );

            if($user) {
                $output['valide'] = true;
                $output['user'] = $user;
            }
        }

        return $output;
    }

    public function getCurrentSessionUser(): ?User {
        $session_user = Session::get(Session_Dictionnary::USER);
        if(!$session_user || !($session_user instanceof SessionUser)) { return null; }
        $credentials = $session_user->toCredentials();
        return $this->loginFromSession($credentials['email'], $credentials['password']);
    }
}