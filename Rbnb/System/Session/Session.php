<?php

namespace Rbnb\System\Session;

abstract class Session {
    public static function get(string $key) {
        if(empty($_SESSION[$key])) { return null; }
        return $_SESSION[$key];
    }

    public static function set(string $key, $value): void {
        $_SESSION[$key] = $value;
    }

    public static function destroy(): void {
        session_destroy();
        unset($_SESSION);
    }
}