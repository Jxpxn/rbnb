<?php

namespace Rbnb\System\Session;

class SessionUser {
    public $key;

    public function __construct(string $email, string $password) {
        $this->key = self::toKey($email, $password);
    }

    public function toCredentials(): array {
        return self::ConverttoCredentials($this->key);
    }

    public static function toKey(string $email, string $password): string {
        return $email . '::' . $password;
    }

    public static function ConverttoCredentials(string $key): array {
        $output = [
            'email' => '',
            'password' => ''
        ];

        $args = explode('::', $key);
        if(count($args) == 2) {
            $output['email'] = $args[0];
            $output['password'] = $args[1];
        }

        return $output;
    }
}