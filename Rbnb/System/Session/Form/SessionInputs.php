<?php

namespace Rbnb\System\Session\Form;

class SessionInputs {
    #region Static
    #region Error_Dictionnary
    public const EMPTY_EMAIL = 1;
    public const EMPTY_PASSWORD = 2;
    public const EMPTY_FIELD = 4;
    public const INVALIDE_CREDENTIALS = 3;
    public const EMAIL_EXIST = 5;
    public const OTHER_ERROR = 6;
    public const INVALIDE_FIELD = 7;
    #endregion

    #region Methods
    public static function constructError(SessionInputs $arg, int $code, string $key = 'default'): void {
        switch($code) {
            case SessionInputs::EMPTY_EMAIL:
                $arg->setError(
                    'email',
                    'Veuillez saisir un email'
                );
                break;
            case SessionInputs::EMPTY_PASSWORD:
                $arg->setError(
                    'password',
                    'Veuillez saisir un mot de passe'
                );
                break;
            case SessionInputs::EMPTY_FIELD:
                $arg->setError(
                    $key,
                    'Veuillez remplir ce champ'
                );
                break;
            case SessionInputs::INVALIDE_CREDENTIALS:
                $arg->setError(
                    'form',
                    'Email ou mot de passe incorrecte'
                );
                break;
            case SessionInputs::EMAIL_EXIST:
                $arg->setError(
                    'email',
                    'Cette email est déja utiliser'
                );
                break;
            case SessionInputs::OTHER_ERROR:
                $arg->setError(
                    'form',
                    "Une erreur s'est produite"
                );
                break;
            case SessionInputs::INVALIDE_FIELD:
                $arg->setError(
                    $key,
                    "Ce champ est incorrecte"
                );
                break;
            default:
                break;
        }
    }
    #endregion
    #endregion

    private $inputs_array = [];
    private $inputs_values = [];
    private $inputs_errors = [];

    public function __construct(array $values = [], array $errors = []) {
        $this->inputs_values = $values;
        $this->inputs_errors = $errors;
    }

    #region Set
    public function setError(string $key, string $error): void {
        $this->inputs_errors[$key] = $error;
    }

    public function setValue(string $key, string $value): void {
        $this->inputs_values[$key] = $value;
    }

    public function setArray(string $key, array $array): void {
        $this->inputs_array[$key] = $array;
    }
    #endregion

    #region Get
    public function getError(string $key): string {
        if(!$this->hasError($key)) { return ''; }
        return $this->inputs_errors[$key];
    }

    public function getValue(string $key): string {
        if(empty($this->inputs_values[$key])) { return ''; }
        return $this->inputs_values[$key];
    }

    public function getArray(string $key): array {
        if(empty($this->inputs_array[$key])) { return []; }
        return $this->inputs_array[$key];
    }
    #endregion

    #region Destroy
    public function destroyError(string $key): void {
        if($this->hasError($key, false)) {
            unset($this->inputs_errors[$key]);
        }
    }

    public function destroyValue(string $key): void {
        if($this->hasValue($key, false)) {
            unset($this->inputs_values[$key]);
        }
    }

    public function destroyArray(string $key): void {
        if($this->hasArray($key, false)) {
            unset($this->inputs_array[$key]);
        }
    }
    #endregion

    #region Check
    public function hasError(string $key, bool $check_string = true): bool {
        return !empty($this->inputs_errors[$key]) && !($check_string && $this->inputs_errors[$key] == '');
    }

    public function hasValue(string $key, bool $check_string = true): bool {
        return !empty($this->inputs_values[$key]) && !($check_string && $this->inputs_values[$key] == '');
    }

    public function hasArray(string $key, bool $check_array = true): bool {
        return !empty($this->inputs_array[$key]) && !($check_array && count($this->inputs_array[$key]) <= 0);
    }
    #endregion
}