<?php

namespace Rbnb\System\Database;

use \PDO;
use PDOStatement;

abstract class Repository {
    protected $pdo = null;

    protected abstract function getTable(): string;

    public function __construct( PDO $connection ) {
        $this->pdo = $connection;
    }

    protected function createTable(): void {}

    protected function insert(array $params): int {
        $query = 'INSERT INTO ' . $this->getTable() . ' (';
        $params_length = count($params);
        if($params_length > 0) {
            $values = '';
            $index = 0;
            foreach($params as $key => $value) {
                $index++;
                $query .= ' ' . $key;
                $values .= ' :' . $key;

                if($index < $params_length) {
                    $query .= ', ';
                    $values .= ', ';
                }
            }
            $query .= ') VALUES (' . $values . ');';
            $stmt = $this->pdo->prepare($query);
            $success = $stmt->execute($params);
            if($success) {
                return $this->pdo->lastInsertId();
            }
        }
        return -1;
    }

    protected function read(array $params = [], bool $first_only = false, array $extra = []): ?PDOStatement {
        $query = 'SELECT * FROM ' . $this->getTable();
        $params_length = count($params);
        $extra_length = count($extra);
        if($params_length > 0) {
            $query .= ' WHERE';
            $index = 0;
            foreach($params as $key => $value) {
                $index++;
                $query .= ' ' . $key . '=:' . $key;
                if($index < $params_length) {
                    $query .= ' AND';
                }
            }
        }
        if($first_only) {
            $query .= ' LIMIT 1';
        }
        if($extra_length > 0) {
            $query .= ' ';
            $index = 0;
            foreach($extra as $item) {
                $index++;
                $query .= $item;
                if($index < $extra_length) {
                    $query .= ' ';
                }
            }
        }
        $query .= ';';
        $stmt = $this->pdo->prepare($query);
        $result = $stmt->execute(count($params) > 0 ? $params : []);
        if($result) {
            return $stmt;
        }
        return null;
    }

    protected function update(array $where, array $params): int {
        $params_length = count($params);
        if($params_length > 0) {
            $query = 'UPDATE ' . $this->getTable() . ' SET';
            #region SET
            $_index = 0;
            foreach($params as $key => $value) {
                $_index++;
                $query .= ' ' . $key . '=:' . $key;
                if($_index < $params_length) {
                    $query .= ',';
                }
            }
            #endregion
            #region WHERE
            $where_length = count($where);           
            if($where_length > 0) {
                $index = 0;
                $query .= ' WHERE';
                foreach($where as $key => $value) {
                    $index++;
                    $query .= ' ' . $key . '=:' . $key;
                    $params[$key] = $value;
                    if($index < $where_length) {
                        $query .= ' AND';
                    }
                }
            }
            #endregion
            $stmt = $this->pdo->prepare($query);
            $result = $stmt->execute($params);
            return $stmt->rowCount();
        }
        return 0;
    }

    protected function remove(array $params): int {
        $query = 'DELETE FROM ' . $this->getTable();
        $params_length = count($params);
        if($params_length > 0) {
            $query .= ' WHERE';
            $index = 0;
            foreach($params as $key => $value) {
                $index++;
                $query .= ' ' . $key . '=:' . $key;
                if($index < $params_length) {
                    $query .= ' AND';
                }
            }
        }
        $stmt = $this->pdo->prepare($query);
        $result = $stmt->execute($params);
        if($result) {
            return $stmt->rowCount();
        }
        return 0;
    }

    protected function checkCommand(string $query, string $command): bool {
        $output = false;
        if(trim($query) != '') {
            $query_exploded = \explode(' ', $query);
            if($query_exploded) {
                $output = count($query_exploded) > 0 ? $query_exploded[0] == $command : false;
            }
        }
        return $output;
    }
}