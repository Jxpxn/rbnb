<?php

namespace Rbnb\System\Database;

use Rbnb\Settings;

use \PDO;

class Database {
    private static $instance = null;
    public static function getDatabase(): self {
        if(!self::$instance) { self::$instance = new self(); }
        return self::$instance;
    }

    private $pdo = null;
    public function getPDO(): ?PDO { return $this->pdo; }

    private function __construct() {
        $this->initPDO();
    }

    private function initPDO(): void {
        $s = Settings::instance();

        $db_host = $s->get( 'db_host' );
		$db_user = $s->get( 'db_user' );
		$db_pass = $s->get( 'db_pass' );
		$db_name = $s->get( 'db_name' );

		$dsn = 'mysql:dbname=' . $db_name . ';host=' . $db_host;
		$options = [
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
		];

		$pdo = null;

		try {
			$pdo = new PDO( $dsn, $db_user, $db_pass, $options );
		}
		catch( PDOException $e ) {
			return;
		}

        $this->pdo = $pdo;
    }

    #region Do_not_instanciate_me_plz_:/
    private function __wakeup() {}
    private function __clone() {}
    #endregion
}