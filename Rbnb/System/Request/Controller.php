<?php

namespace Rbnb\System\Request;

use Rbnb\Rbnb;

abstract class Controller {
    protected $twig = null;

    public function __construct() {
        $this->twig = Rbnb::instance()->getTwig();
    }   
}