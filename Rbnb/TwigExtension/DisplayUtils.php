<?php

namespace Rbnb\TwigExtension;

use Rbnb\Database\Model\Equipement;
use Rbnb\Database\Model\RoomEquipement;
use Rbnb\Settings;

use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;

class DisplayUtils extends AbstractExtension
{
	public function getFunctions()
	{
		return [
			new TwigFunction( 'getEquipementsDisplay', [ $this, 'getEquipementsDisplay' ] )
		];
	}

	public function getEquipementsDisplay( array $equipements ): string
	{
		$output = '';

		$count = count($equipements);
		if($count == 0) {
		    return 'Aucun';
        }
		for($i = 0; $i < $count; $i++) {
		    $item = $equipements[$i];
		    if($item instanceof RoomEquipement) {
		        $item = $item->getEquipement();
            }

		    if($item instanceof Equipement) {
		        $output .= ucfirst($item->name);

		        if($i < $count - 1) {
		            $output .= ' · ';
                }
            }
        }

		return $output;
	}
}