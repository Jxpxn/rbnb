<?php

namespace Rbnb\TwigExtension;

use Rbnb\Settings;

use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;

use \DateTime;

class StringUtils extends AbstractExtension
{
	public function getFunctions()
	{
		return [
			new TwigFunction( 'ucFirst', [ $this, 'ucFirst' ] ),
			new TwigFunction( 'getDate', [ $this, 'getDate' ] ),
			new TwigFunction( 'getDifferenceBetweenDate', [ $this, 'getDifferenceBetweenDate' ] ),
			new TwigFunction( 'getHtmlText', [ $this, 'getHtmlText' ] )
		];
	}

	public function ucFirst( string $value ): string
	{
		return ucfirst($value);
	}

	public function getDate( string $value ): ?DateTime {
		return new DateTime( $value );
	}

	public function getDifferenceBetweenDate( DateTime $a, DateTime $b): string {
		$temp = date_diff($a, $b);
		return $temp->format('%d');
	}

	public function getHtmlText( string $text ): string {
		return nl2br($text);
	}
}