<?php

namespace Rbnb\TwigExtension;

use Rbnb\Rbnb;
use Rbnb\Settings;

use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;

use Rbnb\Database\Model\User;
use Rbnb\Database\Model\Role;
use Rbnb\Database\Model\Profile; 
use Rbnb\Database\Model\Credential;

class TopbarUtils extends AbstractExtension
{
	public function getFunctions()
	{
		return [
			new TwigFunction( 'drawTopBar', [ $this, 'drawTopBar' ] )
		];
	}

	public function drawTopBar( $user ): void {
        if($user instanceof User) {
            if($user->role_id == Role::ID_USER) {
                echo $this->getUserTopBar($user);
            }
            else if($user->role_id == Role::ID_ANNOUNCER) {
                echo $this->getAnnouncerTopBar($user);
            }
        }
    }
    
    private function getAnnouncerTopBar(User $user): string {
        $router = Rbnb::instance()->getRouter();
        $profile = $user->getProfile();
        $credential = $user->getCredential();
        
        $code = sprintf('
            <div class="dropdown show mx-3">
                <a class="" style="display: inline-block" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="%s" class="profile-picture">
                </a>

                <div id="user-dropdown" class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <p class="text-center">%s %s</p>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="%s">Tableau de bord</a>
                    <a class="dropdown-item" href="%s">Mes Réservations</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="%s">Mon Profil</a>
                    <a class="dropdown-item" href="%s">Paramètres</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="%s">Déconnexion</a>
                </div>
            </div>',
            URLUtils::getAssetsUrl_($profile->avatar_filename),
            $credential->first_name,
            $credential->last_name,
            $router->url('announcer_dashboard'),
            $router->url('user_reservations'),
            '/profile/' . $user->id,
            $router->url('user_settings'),
            $router->url('logout')
        );

        return $code;
    }

	private function getUserTopBar(User $user): string {
        $router = Rbnb::instance()->getRouter();
        $profile = $user->getProfile();
        $credential = $user->getCredential();
        
        $code = sprintf('
            <div class="dropdown show mx-3">
                <a class="" style="display: inline-block" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="%s" class="profile-picture">
                </a>

                <div id="user-dropdown" class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <p class="text-center">%s %s</p>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="%s">Mes Réservations</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="%s">Mon Profil</a>
                    <a class="dropdown-item" href="%s">Paramètres</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="%s">Déconnexion</a>
                </div>
            </div>',
            URLUtils::getAssetsUrl_($profile->avatar_filename),
            $credential->first_name,
            $credential->last_name,
            $router->url('user_reservations'),
            '/profile/' . $user->id,
            $router->url('user_settings'),
            $router->url('logout')
        );

        return $code;
    }
}