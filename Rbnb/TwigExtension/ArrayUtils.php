<?php

namespace Rbnb\TwigExtension;

use Rbnb\Database\Model\Equipement;
use Rbnb\Database\Model\RoomEquipement;
use Rbnb\Settings;

use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;

class ArrayUtils extends AbstractExtension
{
	public function getFunctions()
	{
		return [
			new TwigFunction( 'getArrayCount', [ $this, 'getArrayCount' ] )
		];
	}

	public function getArrayCount( array $equipements ): int
	{
		return count($equipements);
	}
}