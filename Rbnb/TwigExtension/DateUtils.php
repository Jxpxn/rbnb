<?php

namespace Rbnb\TwigExtension;

use Rbnb\Database\Model\Equipement;
use Rbnb\Database\Model\RoomEquipement;
use Rbnb\Settings;

use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;

use \DateTime;

class DateUtils extends AbstractExtension
{
	public function getFunctions()
	{
		return [
            new TwigFunction( 'getDate', [ $this, 'getDate' ] ),
            new TwigFunction( 'getDifferenceBetweenDate', [ $this, 'getDifferenceBetweenDate' ] ),
            new TwigFunction( 'dateIsOutdated', [ $this, 'dateIsOutdated' ] )
		];
    }
    
    public function getDate( string $value ): ?DateTime {
		return new DateTime( $value );
	}

	public function getDifferenceBetweenDate( DateTime $a, DateTime $b): string {
		$temp = date_diff($a, $b);
		return $temp->format('%d');
    }
    
    public function dateIsOutdated( DateTime $a ): bool {
        return new DateTime() >= $a;
    }
}