<?php

namespace Rbnb\TwigExtension;

use Rbnb\Settings;

use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;

class HTMLUtils extends AbstractExtension
{
	public function getFunctions()
	{
		return [
			new TwigFunction( 'htmlTitle', [ $this, 'getHTMLTitle' ] ),
			new TwigFunction( 'siteFullName', [ $this, 'getSiteFullName' ] )
		];
	}

	public function getHTMLTitle( $value ): string
	{
		return sprintf(
			'%s - %s',
			$value,
			Settings::instance()->get( 'site_name' )
		);
	}

	public function getSiteFullName(): string
	{
		return Settings::instance()->get( 'site_full_name' );
	}
}