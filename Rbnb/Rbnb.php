<?php

namespace Rbnb;

use Rbnb\TwigExtension\ArrayUtils;
use Rbnb\TwigExtension\DisplayUtils;
use Rbnb\TwigExtension\HTMLUtils;
use Rbnb\TwigExtension\URLUtils;
use Rbnb\TwigExtension\TopbarUtils;
use Rbnb\TwigExtension\StringUtils;
use Rbnb\TwigExtension\DateUtils;
use Rbnb\Request\Routes;
use Rbnb\System\Authentification\Auth;

use MiladRahimi\PhpRouter\Router;

use Rbnb\Request\Middleware\AuthMiddleware;
use Rbnb\Request\Middleware\VisitorMiddleware;
use Rbnb\Request\Middleware\UserMiddleware;
use Rbnb\Request\Middleware\AnnouncerMiddleware;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class Rbnb
{
	private $router = null;
	public function getRouter(): ?Router { return $this->router; }

	private $twig = null;
	public function getTwig(): ?Environment { return $this->twig; }

	private $auth = null;
	public function getAuth(): ?Auth { return $this->auth;}

	private static $instance = null;

	public static function instance(): self
	{
		if( is_null( self::$instance ) )
			self::$instance = new self();

		return self::$instance;
	}

	public function start(): void
	{
		session_start();

		$this->defineAuth();
		$this->defineTwig();
		$this->defineRouter();
		$this->launchRouter();
	}

	private function launchRouter(): void {
		if($this->router) { $this->router->dispatch(); }
	}

	private function defineRouter(): void {
		$this->router = new Router();

		$attr_visitor = [
			'namespace' => 'Rbnb\Request\Controller',
			'middleware' => [ VisitorMiddleware::class ]
		];

		$attr_user = [
			'namespace' => 'Rbnb\Request\Controller',
			'middleware' => [ AuthMiddleware::class ]
		];

		$attr_announcer = [
			'namespace' => 'Rbnb\Request\Controller',
			'middleware' => [ AuthMiddleware::class, AnnouncerMiddleware::class ]
		];

		$this->router
			->group( $attr_visitor, Routes::visitor() )
			->group( $attr_announcer, Routes::announcer() )
			->group( $attr_user, Routes::user() )
		;
	}

	private function defineTwig(): void {
		$this->twig = new Environment(
			new FilesystemLoader(
				ROOT_PATH . 'views',
				ROOT_PATH . 'views/logged',
				ROOT_PATH . 'views/user',
				ROOT_PATH . 'views/announcer',
				ROOT_PATH . 'views/visitor'
			)
		);

		// Extensions
		$this->twig->addExtension(new HTMLUtils());
		$this->twig->addExtension(new URLUtils());
		$this->twig->addExtension(new TopbarUtils());
		$this->twig->addExtension(new StringUtils());
		$this->twig->addExtension(new DisplayUtils());
		$this->twig->addExtension(new ArrayUtils());
		$this->twig->addExtension(new DateUtils());
	}

	private function defineAuth(): void {
		$this->auth = new Auth();
	}

	#region Protection
	private function __construct() {}
	private function __clone() {}
	private function __wakeup() {}
	#endregion
}