<?php

namespace Rbnb\Utils;

abstract class ArrayUtils {
    public static function appendArray(array $a, array $b, bool $assoc = false): array {
        $output = $a;
        if(count($b) > 0) {
            if($assoc) {
                foreach($b as $key => $value) {
                    $output[$key] = $value;
                }
            }
            else {
                foreach($b as $value) {
                    array_push($output, $value);
                }
            }
        }
        return $output;
    }

    public static function subArray(array $a, int $min, int $max = -1): array {
        $output = [];
        $a_len = count($a);
        if($a_len > 0 && $min < $a_len) {
            $max = $max < $a_len && $max >= 0 ? $max : $a_len - 1;
            for($i = $min; $i <= $max; $i++) {
                array_push($output, $a[$i]);
            }
        }
        return $output;
    }

    public static function hasKeys(array $a, array $keys, bool $check_isnull = false): bool {
        $a_len = count($a);
        $keys_len = count($keys);
        $output = $keys_len == 0;
        if($a_len > 0 && $keys_len > 0) {
            foreach($keys as $key) {
                $temp = false;
                foreach($a as $k => $v) {
                    if($k == $key && !($check_isnull && is_null($v))) {
                        $temp = true;
                        break;
                    }
                }
                if(!$temp) {
                    $output = false;
                    break;
                }
            }
        }
        return $output;
    }

    public static function contain(array $a, $target_value, bool $assoc = false, bool $is_exact_equal = false): bool {
        $output = false;
        if(count($a) > 0) {
            if($assoc) {
                foreach($a as $key => $value) {
                    if((!$is_exact_equal && $value == $target_value) || ($is_exact_equal && $value === $is_exact_equal)) {
                        $output = true;
                        break;
                    }
                }
            }
            else {
                foreach($a as $value) {
                    if((!$is_exact_equal && $value == $target_value) || ($is_exact_equal && $value === $is_exact_equal)) {
                        $output = true;
                        break;
                    }
                }
            }
        }
        return $output;
    }

    public static function arrayHasSameValues(array $a, array $b) {
        $a_count = count($a);
        $b_count = count($b);

        $output = $a_count == 0 && $b_count == 0;

        if($a_count > 0 && $b_count > 0) {
            $output = true;
            foreach($a as $item) {
                if(!self::contain($b, $item)) {
                    $output = false;
                    break;
                }
            }

            if($output) {
                foreach($b as $item) {
                    if(!self::contain($a, $item)) {
                        $output = false;
                        break;
                    }
                }
            }
        }

        return $output;
    }
}