<?php

namespace Rbnb\Utils;

abstract class MathHelper {
    public static function moyenne(array $numbers): float {
        $output = 0;
        if(count($numbers) > 0) {
            foreach($numbers as $number) {
                $output += $number;
            }
            $output /= count($numbers);
        }
        return $output;
    }
}