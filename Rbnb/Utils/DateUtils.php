<?php

namespace Rbnb\Utils;

use \DateTime;

abstract class DateUtils {
    public static function dateIsInRange(DateTime $a, DateTime $b, DateTime $c): bool {
        return $c >= $a && $c <= $b;
    }

    public static function isInRange(string $a, string $b, string $c): bool {
        $a_ = new DateTime($a);
        $b_ = new DateTime($b);
        $c_ = $c == '' ? new DateTime() : new DateTime($c);
        return self::dateIsInRange($a_, $b_, $c_);
    }
}