<?php

namespace Rbnb\Utils;

abstract class StringUtils {
    public const alphabet = 'azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN';

    public static function isUpperCase(string $arg): bool {
        $output = false;
        if(trim($arg) != '') {
            $output = true;
            $alphabet = 'azertyuiopqsdfghjklmwxcvbn';
            $alphabet_len = strlen($alphabet);
            $arg_len = strlen($arg);
            for($i = 0; $i < $arg; $i++) {
                $char = $arg[$i];
                $temp = true;
                for($a = 0; $a < $alphabet_len; $a++) {
                    if($char == $alphabet[$a]) {
                        $temp = false;
                        break;
                    }
                }
                if(!$temp) {
                    $output = true;
                    break;
                }
            }
        }
        return $output;
    }

    public static function toLowerCase(string $arg): string {
        $output = '';
        if($arg != '') {
            $alphabet_len = strlen(self::alphabet);
            $arg_len = strlen($arg);
            for($a = 0; $a < $arg_len; $a++) {
                $char = $arg[$a];
                for($i = 26; $i < $alphabet_len; $i++) {
                    if($char == self::alphabet) {
                        $char = self::alphabet[$i - 26];
                        break;
                    }
                }
                $output .= $char;
            }
        }
        return $output;
    }

    public static function toUpperCase(string $arg): string {
        $output = '';
        if($arg != '') {
            $alphabet_len = strlen(self::alphabet);
            $arg_len = strlen($arg);
            for($a = 0; $a < $arg_len; $a++) {
                $char = $arg[$a];
                for($i = 0; $i < 26; $i++) {
                    if($char == self::alphabet) {
                        $char = self::alphabet[$i + 26];
                        break;
                    }
                }
                $output .= $char;
            }
        }
        return $output;
    }
}