<?php

namespace Rbnb\Database\Repository;

use Rbnb\System\Database\Repository;
use Rbnb\Database\Model\Rating;

use \PDO;

class RatingRepository extends Repository {
    protected function getTable(): string { 
        return 'rating';
    }

    public function getById(int $id): ?Rating {
        $result = $this->read(['id' => $id], true);
        if(!$result) { return null; }
        $result_fetched = $result->fetch();
        return $result_fetched ? new Rating($result_fetched) : null;
    }

    public function getAllByRoom(int $room_id, bool $by_date = false): array {
        $output = [];

        $extra = [];
        if($by_date) {
            array_push($extra,
                'ORDER BY created_time DESC'
            );
        }

        $result = $this->read(['room_id' => $room_id], false, $extra);
        if(!$result) { return $output; }
        $result_fetched = $result->fetchAll();
        foreach($result_fetched as $fetch) {
            array_push($output, new Rating($fetch));
        }

        return $output;
    }

    public function getAllByUser(int $user_id, bool $by_date = false): array {
        $output = [];

        $extra = [];
        if($by_date) {
            array_push($extra,
                'ORDER BY created_time DESC'
            );
        }

        $result = $this->read(['user_id' => $user_id], false, $extra);
        if(!$result) { return $output; }
        $result_fetched = $result->fetchAll();
        foreach($result_fetched as $fetch) {
            array_push($output, new Rating($fetch));
        }

        return $output;
    }

    public function registerRating( int $room_id, int $user_id, int $rate, string $comment ): int {
        return $this->insert([
            'room_id' => $room_id,
            'user_id' => $user_id,
            'rate' => $rate,
            'comment' => $comment
        ]);
    }

    public function updateRating( Rating $rating ): int {
        $to_update = $rating->toArray();
        return $this->update(
            ['id' => $rating->id],
            $rating->toArray()
        );
    }

    public function removeRating( Rating $rating_id ): int {
        return $this->remove(
            [
                'id' => $rating_id
            ]
        );
    }

    public function removeByRoom( int $room_id ): int {
        return $this->remove(
            [
                'room_id' => $room_id
            ]
        );
    }

    public function removeByUser( int $user_id ): int {
        return $this->remove(
            [
                'user_id' => $user_id
            ]
        );
    }
}