<?php

namespace Rbnb\Database\Repository;

use Rbnb\System\Database\Repository;
use Rbnb\Database\Model\Profile;

use \PDO;

class ProfileRepository extends Repository {
    protected function getTable(): string { 
        return 'profile';
    }

    public function getById(int $id): ?Profile {
        $result = $this->read(['id' => $id], true);
        if(!$result) { return null; }
        $result_fetched = $result->fetch();
        return $result_fetched ? new Profile($result_fetched) : null;
    }

    public function registerProfile( string $avatar_filename, string $bio ): int {
        return $this->insert([
            'avatar_filename' => $avatar_filename,
            'bio' => $bio
        ]);
    }

    public function updateProfile( Profile $profile ): int {
        $to_update = $profile->toArray();
        return $this->update(
            ['id' => $profile->id],
            $profile->toArray()
        );
    }

    public function removeProfile( int $profile_id ): int {
        return $this->remove(
            [
                'id' => $profile_id
            ]
        );
    }
}