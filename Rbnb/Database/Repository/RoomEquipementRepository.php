<?php

namespace Rbnb\Database\Repository;

use Rbnb\System\Database\Repository;
use Rbnb\Database\Model\RoomEquipement;

use \PDO;

class RoomEquipementRepository extends Repository {
    protected function getTable(): string { 
        return 'rooms_equipements';
    }

    public function getById(int $id): ?RoomEquipement {
        $result = $this->read(['id' => $id], true);
        if(!$result) { return null; }
        $result_fetched = $result->fetch();
        return $result_fetched ? new RoomEquipement($result_fetched) : null;
    } 

    public function getByRoom(int $room_id): ?RoomEquipement {
        $result = $this->read(['room_id' => $room_id], true);
        if(!$result) { return null; }
        $result_fetched = $result->fetch();
        return $result_fetched ? new RoomEquipement($result_fetched) : null;
    }

    public function getAllByRoom(int $room_id): array {
        $output = [];

        $result = $this->read(['room_id' => $room_id]);
        if(!$result) { return $output; }
        $result_fetched = $result->fetchAll();

        if($result_fetched && count($result_fetched) > 0) {
            foreach($result_fetched as $fetch) {
                array_push($output, new RoomEquipement($fetch));
            }
        }

        return $output;
    }

    public function registerRoomEquipement(int $equipement_id, int $room_id): int {
        return $this->insert([
            'equipement_id' => $equipement_id,
            'room_id' => $room_id
        ]);
    }

    public function removeRoomEquipement(int $room_equipement_id): int {
        return $this->remove([
            'id' => $room_equipement_id
        ]);
    }

    public function removeRoomEquipementbyRoom(int $room_id): int {
        return $this->remove([
            'room_id' => $room_id
        ]);
    }

    public function removeRoomEquipementbyEquipement(int $equipement_id): int {
        return $this->remove([
            'equipement_id' => $equipement_id
        ]);
    }
}