<?php

namespace Rbnb\Database\Repository;

use Rbnb\System\Database\Repository;
use Rbnb\Database\Model\Announcer;
use Rbnb\Database\Model\User;
use Rbnb\Database\Model\Role;

use \PDO;

class UserRepository extends Repository {
    protected function getTable(): string { 
        return 'user';
    }

    public function getByEmail(string $email): ?User {
        $result = $this->read(['email' => $email], true);
        if(!$result) { return null; }
        $result_fetched = $result->fetch();
        if($result_fetched) {
            if($result_fetched['role_id'] == Role::ID_ANNOUNCER) {
                return new Announcer($result_fetched);
            }
            else {
                return new User($result_fetched);
            }
        }
        return null;
    }

    public function getById(int $id): ?User {
        $result = $this->read(['id' => $id], true);
        if(!$result) { return null; }
        $result_fetched = $result->fetch();
        if($result_fetched) {
            if($result_fetched['role_id'] == Role::ID_ANNOUNCER) {
                return new Announcer($result_fetched);
            }
            else {
                return new User($result_fetched);
            }
        }
        return null;
    }

    public function getAll(): array {
        $output = [];

        $result = $this->read();
        if(!$result) { return $output; }
        $result_fetched = $result->fetchAll();
        
        if($result_fetched && count($result_fetched) > 0) {
            foreach($result_fetched as $fetch) {
                $to_add = null;

                if($fetch['role_id'] == Role::ID_ANNOUNCER) {
                    $to_add = new Announcer($fetch);
                }
                else {
                    $to_add = new User($fetch);
                }

                array_push($output, $to_add);
            }
        }
        return $output;
    }

    public function registerUser( string $email, string $password, int $role_id, int $credential_id, int $profile_id ): int {
        return $this->insert([
            'email' => $email,
            'password' => $password,
            'role_id' => $role_id,
            'credentials_id' => $credential_id,
            'profile_id' => $profile_id
        ]);
    }

    public function updateUser( User $user ): int {
        return $this->update(
            ['id' => $user->id],
            $user->toArray()
        );
    }
}