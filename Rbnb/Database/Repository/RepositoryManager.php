<?php

namespace Rbnb\Database\Repository;

use Rbnb\System\Database\Model;
use Rbnb\System\Database\Database;
use Rbnb\System\Database\Repository;

class RepositoryManager {
    private static $instance = null;

    public static function instance(): self {
        if(!self::$instance) { self::$instance = new self(); }
        return self::$instance;
    }

    private $pdo = null;
    private $repositorys = [];

    private function __construct() {
        $this->pdo = Database::getDatabase()->getPDO();

        $this->initRepositorys();
    }

    private function initRepositorys(): void {
        $m = $this->pdo;

        $this->registerRepository('user', new UserRepository( $m ));
        $this->registerRepository('role', new RoleRepository( $m ));
        $this->registerRepository('profile', new ProfileRepository( $m ));
        $this->registerRepository('credential', new CredentialsRepository( $m ));
        $this->registerRepository('room_type', new Room_TypeRepository( $m ));
        $this->registerRepository('room', new RoomRepository( $m ));
        $this->registerRepository('equipement', new EquipementRepository( $m ));
        $this->registerRepository('room_equipement', new RoomEquipementRepository( $m ));
        $this->registerRepository('rating', new RatingRepository( $m ));
        $this->registerRepository('reservation', new ReservationRepository( $m ));
    }

    private function registerRepository(string $key, Repository $repository) {
        $this->repositorys[$key] = $repository;
    }

    public function getRepository( string $key ): ?Repository {
        return empty($this->repositorys[$key]) ? null : $this->repositorys[$key];
    }

    #region Protection
    private function __wakeup() {}
    private function __clone() {}
    #endregion
}