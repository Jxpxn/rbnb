<?php

namespace Rbnb\Database\Repository;

use Rbnb\System\Database\Repository;
use Rbnb\Database\Model\Equipement;

use \PDO;

class EquipementRepository extends Repository {
    protected function getTable(): string { 
        return 'equipement';
    }

    public function getById(int $id): ?Equipement {
        $result = $this->read(['id' => $id], true);
        if(!$result) { return null; }
        $result_fetched = $result->fetch();
        return $result_fetched ? new Equipement($result_fetched) : null;
    } 

    public function getAll(): array {
        $output = [];

        $result = $this->read();
        if(!$result) { return $output; }
        $result_fetched = $result->fetchAll();

        if($result_fetched && count($result_fetched) > 0) {
            foreach($result_fetched as $fetch) {
                array_push($output, new Equipement($fetch));
            }
        }

        return $output;
    }

    public function idExist(int $id): bool {
        return !is_null($this->getById($id));
    }
}