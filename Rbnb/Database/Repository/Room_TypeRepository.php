<?php

namespace Rbnb\Database\Repository;

use Rbnb\System\Database\Repository;
use Rbnb\Database\Model\Room_Type;

use \PDO;

class Room_TypeRepository extends Repository {
    protected function getTable(): string { 
        return 'room_type';
    }

    public function getById(int $id): ?Room_Type {
        $result = $this->read(['id' => $id], true);
        if(!$result) { return null; }
        $result_fetched = $result->fetch();
        return $result_fetched ? new Room_Type($result_fetched) : null;
    }

    public function getAll(): array {
        $output = [];

        $result = $this->read();
        if(!$result) { return $output; }
        $result_fetched = $result->fetchAll();

        if($result_fetched && count($result_fetched) > 0) {
            foreach($result_fetched as $fetch) {
                array_push($output, new Room_Type($fetch));
            }
        }
        return $output;
    }

    public function idExist(int $id): bool {
        $result = $this->read(['id' => $id], true);
        if(!$result) { return false; }
        $result_fetched = $result->fetch();
        return $result_fetched && count($result_fetched) > 0;
    }
}