<?php

namespace Rbnb\Database\Repository;

use Rbnb\System\Database\Repository;
use Rbnb\Database\Model\Reservation;

use \PDO;

class ReservationRepository extends Repository {
    protected function getTable(): string { 
        return 'reservation';
    }

    public function getById(int $id): ?Reservation {
        $result = $this->read(['id' => $id], true);
        if(!$result) { return null; }
        $result_fetched = $result->fetch();
        return $result_fetched ? new Reservation($result_fetched) : null;
    }

    public function getAllByRoom(int $room_id, bool $by_date = false): array {
        $output = [];

        $extra = [];
        if($by_date) {
            array_push($extra,
                'ORDER BY start_time DESC'
            );
        }

        $result = $this->read(['room_id' => $room_id], false, $extra);
        if(!$result) { return $output; }
        $result_fetched = $result->fetchAll();
        foreach($result_fetched as $fetch) {
            array_push($output, new Reservation($fetch));
        }

        return $output;
    }

    public function getAllByUser(int $user_id, bool $by_date = false): array {
        $output = [];

        $extra = [];
        if($by_date) {
            array_push($extra,
                'ORDER BY start_time DESC'
            );
        }

        $result = $this->read(['user_id' => $user_id], false, $extra);
        if(!$result) { return $output; }
        $result_fetched = $result->fetchAll();
        foreach($result_fetched as $fetch) {
            array_push($output, new Reservation($fetch));
        }

        return $output;
    }

    public function registerReservation( int $room_id, int $user_id, $start_time, $end_time ): int {
        return $this->insert([
            'room_id' => $room_id,
            'user_id' => $user_id,
            'start_time' => $start_time,
            'end_time' => $end_time
        ]);
    }

    public function updateReservation( Reservation $reservation ): int {
        $to_update = $reservation->toArray();
        return $this->update(
            ['id' => $reservation->id],
            $reservation->toArray()
        );
    }

    public function removeReservation( int $reservation_id ): int {
        return $this->remove(
            [
                'id' => $reservation_id
            ]
        );
    }

    public function removeByRoom( int $room_id ): int {
        return $this->remove(
            [
                'room_id' => $room_id
            ]
        );
    }

    public function removeByUser( int $user_id ): int {
        return $this->remove(
            [
                'user_id' => $user_id
            ]
        );
    }
}