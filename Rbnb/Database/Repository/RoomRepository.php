<?php

namespace Rbnb\Database\Repository;

use Rbnb\System\Database\Repository;
use Rbnb\Database\Model\Room;

use \PDO;

class RoomRepository extends Repository {
    protected function getTable(): string { 
        return 'room';
    }

    public function getAll(): array {
        $output = [];

        $result = $this->read();
        if(!$result) { return $output; }
        $result_fetched = $result->fetchAll();

        if($result_fetched && count($result_fetched) > 0) {
            foreach($result_fetched as $fetch) {
                array_push($output, new Room($fetch));
            }
        }
        return $output;
    }

    public function getById(int $id): ?Room {
        $result = $this->read(['id' => $id], true);
        if(!$result) { return null; }
        $result_fetched = $result->fetch();
        return $result_fetched ? new Room($result_fetched) : null;
    } 

    public function getAllByOwner(int $owner_id): array {
        $output = [];

        $result = $this->read(['owner_id' => $owner_id]);
        if(!$result) { return $output; }
        $result_fetched = $result->fetchAll();

        if($result_fetched && count($result_fetched) > 0) {
            foreach($result_fetched as $fetch) {
                array_push($output, new Room($fetch));
            }
        }
        return $output;
    }

    public function registerRoom(
        string $title, int $type_id, int $price, 
        int $size, string $description, int $owner_id, 
        int $slots, string $city, string $country
    ): int {
        return $this->insert([
            'title' => $title,
            'type_id' => $type_id,
            'price' => $price,
            'size' => $size,
            'description' => $description,
            'owner_id' => $owner_id,
            'slots' => $slots,
            'city' => $city,
            'country' => $country
        ]);
    }

    public function updateRoom( Room $room ): int {
        return $this->update(
          [ 'id' => $room->id],
          $room->toArray()
        );
    }

    public function removeRoom(int $room_id): int {
        return $this->remove([
            'id' => $room_id
        ]);
    }
}