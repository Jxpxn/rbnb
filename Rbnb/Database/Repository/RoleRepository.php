<?php

namespace Rbnb\Database\Repository;

use Rbnb\System\Database\Repository;
use Rbnb\Database\Model\Role;

use \PDO;

class RoleRepository extends Repository {
    protected function getTable(): string { 
        return 'role';
    }

    public function getById(int $id): ?Role {
        $result = $this->read(['id' => $id], true);
        if(!$result) { return null; }
        $result_fetched = $result->fetch();
        return $result_fetched ? new Role($result_fetched) : null;
    }

    public function getByName(string $name): ?Role {
        $result = $this->read(['name' => $name], true);
        if(!$result) { return null; }
        $result_fetched = $result->fetch();
        return $result_fetched ? new Role($result_fetched) : null;
    }

    public function getAll(): array {
        $output = [];

        $result = $this->read();
        if(!$result) { return $output; }
        $result_fetched = $result->fetchAll();

        if($result_fetched && count($result_fetched) > 0) {
            foreach($result_fetched as $fetch) {
                array_push($output, new Role($result_fetched));
            }
        }

        return $output;
    }
}