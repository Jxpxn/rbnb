<?php

namespace Rbnb\Database\Repository;

use Rbnb\System\Database\Repository;
use Rbnb\Database\Model\Credential;

use \PDO;

class CredentialsRepository extends Repository {
    protected function getTable(): string { 
        return 'credentials';
    }

    public function getById(int $id): ?Credential {
        $result = $this->read(['id' => $id], true);
        if(!$result) { return null; }
        $result_fetched = $result->fetch();
        return $result_fetched ? new Credential($result_fetched) : null;
    }

    public function registerCredential( string $first_name, string $last_name, $birth_date, string $country ): int {
        return $this->insert([
            'first_name' => $first_name,
            'last_name' => $last_name,
            'birth_date' => $birth_date,
            'country' => $country
        ]);
    }

    public function updateCredential( Credential $credential ): int {
        return $this->update(
            ['id' => $credential->id],
            $credential->toArray()
        );
    }

    public function removeCredential( int $credential_id ): int {
        return $this->remove(
            [
                'id' => $credential_id
            ]
        );
    }
}