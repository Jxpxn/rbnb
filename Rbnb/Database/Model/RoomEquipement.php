<?php

namespace Rbnb\Database\Model;

use Rbnb\Rbnb;
use Rbnb\Database\Repository\RepositoryManager;

use Rbnb\System\Database\Model;

class RoomEquipement extends Model {
    public $equipement_id;
    public $room_id;

    protected $room;
    protected $equipement;

    public function getRoom(): ?Room {
        if(!$this->room) {
            $this->room = RepositoryManager::instance()->getRepository('room')->getById((int)$this->room_id);
        }
        return $this->room;
    }

    public function getEquipement(): ?Equipement {
        if(!$this->equipement) {
            $this->equipement = RepositoryManager::instance()->getRepository('equipement')->getById((int)$this->equipement_id);
        }
        return $this->equipement;
    }

    public function toArray(): array {
        return $this->toFieldsArray([
            'equipement_id',
            'room_id'
        ]);
    }
}