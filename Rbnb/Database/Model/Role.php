<?php

namespace Rbnb\Database\Model;

use Rbnb\System\Database\Model;

class Role extends Model {
    public const ID_USER = 1;
    public const ID_ANNOUNCER = 2;

    public $name;

    public function toArray(): array {
        return $this->toFieldsArray([
            'name'
        ]);
    }
}