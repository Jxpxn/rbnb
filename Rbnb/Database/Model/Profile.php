<?php

namespace Rbnb\Database\Model;

use Rbnb\System\Database\Model;

class Profile extends Model {
    public $avatar_filename;
    public $bio;

    public function toArray(): array {
        return $this->toFieldsArray([
            'avatar_filename',
            'bio'
        ]);
    }
}