<?php

namespace Rbnb\Database\Model;

use Rbnb\System\Database\Model;

class Equipement extends Model {
    public $name;

    public function toArray(): array {
        return $this->toFieldsArray([
            'name'
        ]);
    }
}