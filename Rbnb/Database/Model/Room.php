<?php

namespace Rbnb\Database\Model;

use Rbnb\Rbnb;
use Rbnb\Database\Repository\RepositoryManager;

use Rbnb\Utils\MathHelper;

use Rbnb\System\Database\Model;

class Room extends Model {
    public $title;
    public $type_id;
    public $price;
    public $size;
    public $description;
    public $owner_id;
    public $slots;
    public $city;
    public $country;

    protected $owner = null;
    protected $type = null;
    protected $equipements = null;
    protected $ratings = null;
    protected $reservation = null;

    public function getOwner(): ?User {
        if(!$this->owner) {
            $this->owner = RepositoryManager::instance()->getRepository('user')->getById((int)$this->owner_id);
        }
        return $this->owner;
    }

    public function getType(): ?Room_Type {
        if(!$this->type) {
            $this->type = RepositoryManager::instance()->getRepository('room_type')->getById((int)$this->type_id);
        }
        return $this->type;
    }

    public function getEquipements(): array {
        if(is_null($this->equipements)) {
            $this->equipements = RepositoryManager::instance()->getRepository('room_equipement')->getAllByRoom((int)$this->id);
        }
        return $this->equipements;
    }

    public function getRatings(): array {
        if(is_null($this->ratings)) {
            $this->ratings = RepositoryManager::instance()->getRepository('rating')->getAllByRoom((int)$this->id, true);
        }
        return $this->ratings;
    }

    public function getReservations(): array {
        if(is_null($this->reservation)) {
            $this->reservation = RepositoryManager::instance()->getRepository('reservation')->getAllByRoom((int)$this->id, true);
        }
        return $this->reservation;
    }

    public function getMoyenne(): float {
        $output = 0;

        $ratings = $this->getRatings();
        if(count($ratings) > 0) {
            $temp = [];
            foreach($ratings as $rating) {
                array_push($temp, $rating->rate);
            }
            $output = MathHelper::moyenne($temp);
        }

        return $output;
    }

    public function toArray(): array {
        return $this->toFieldsArray([
            'title',
            'type_id',
            'price',
            'size',
            'description',
            'slots',
            'city',
            'country'
        ]);
    }
}