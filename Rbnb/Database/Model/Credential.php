<?php

namespace Rbnb\Database\Model;

use Rbnb\System\Database\Model;

class Credential extends Model {
    public $first_name;
    public $last_name;
    public $birth_date;
    public $country;

    public function toArray(): array {
        return $this->toFieldsArray([
            'first_name',
            'last_name',
            'birth_date',
            'country'
        ]);
    }
}