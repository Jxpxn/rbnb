<?php

namespace Rbnb\Database\Model;

use Rbnb\System\Database\Model;

class Room_Type extends Model {
    public $name;

    public function toArray(): array {
        return $this->toFieldsArray([
            'name'
        ]);
    }
}