<?php

namespace Rbnb\Database\Model;
use Rbnb\Database\Repository\RepositoryManager;

use Rbnb\System\Database\Model;

class Reservation extends Model {
    public $room_id;
    public $user_id;
    public $start_time;
    public $end_time;

    protected $user = null;
    protected $room = null;

    public function getUser(): ?User {
        if(is_null($this->user)) {
            $this->user = RepositoryManager::instance()->getRepository('user')->getById((int)$this->user_id);
        }
        return $this->user;
    }

    public function getRoom(): ?Room {
        if(is_null($this->room)) {
            $this->room = RepositoryManager::instance()->getRepository('room')->getById((int)$this->room_id);
        }
        return $this->room;
    }

    public function toArray(): array {
        return $this->toFieldsArray([
            'room_id',
            'user_id',
            'start_time',
            'end_time'
        ]);
    }
}