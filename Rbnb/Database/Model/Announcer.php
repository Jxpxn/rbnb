<?php

namespace Rbnb\Database\Model;

use Rbnb\System\Database\Model;
use Rbnb\Database\Repository\RepositoryManager;
use Rbnb\Database\Repository\UserRepository;
use Rbnb\Database\Repository\CredentialsRepository; 
use Rbnb\Database\Repository\ProfileRepository; 

use Rbnb\Utils\MathHelper;
use Rbnb\Utils\DateUtils;

use \DateTime;

class Announcer extends User {
    protected $rooms = null;
    protected $global_reservations = null;
    protected $global_moyenne = null;
    protected $global_current_reservations = null;

    public function getRooms(): array {
        if(is_null($this->rooms)) {
            $this->rooms = RepositoryManager::instance()->getRepository('room')->getAllByOwner((int)$this->id);
        }
        return $this->rooms;
    }

    public function getGlobalReservations(): array {
        if(is_null($this->global_reservations)) {
            $output = [];
            $rooms = $this->getRooms();
            if(count($rooms) > 0) {
                foreach($rooms as $room) {
                    $reservations = $room->getReservations();
                    if(count($reservations) > 0) {
                        foreach($reservations as $reservation) {
                            array_push($output, $reservation);
                        }
                    }
                }
            }
            $this->global_reservations = $output;
        }
        return $this->global_reservations;
    }

    public function getCurrentGlobalReservations(): array {
        if(is_null($this->global_current_reservations)) {
            $output = [];
            $reservations = $this->getGlobalReservations();
            if(count( $reservations ) > 0) {
                $current_time = new DateTime();
                foreach($reservations as $reservation) {
                    $start_time = new DateTime($reservation->start_time);
                    $end_time = new DateTime($reservation->end_time);

                    if($start_time <= $current_time && $end_time >= $current_time) {
                        array_push($output, $reservation);
                    }
                }
            }
            $this->global_current_reservations = $output;
        }
        return $this->global_current_reservations;
    }

    public function getGlobalMoyenne(): float {
        if(is_null($this->global_moyenne)) {
            $moyenne = 0;
            $rooms = $this->getRooms();
            if(count($rooms) > 0) {
                $array = [];
                foreach($rooms as $room) {
                    array_push($array, $room->getMoyenne());
                }
                $moyenne = MathHelper::moyenne($array);
            }
            $this->global_moyenne = $moyenne;
        }
        return $this->global_moyenne;
    }
}