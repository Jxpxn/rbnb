<?php

namespace Rbnb\Database\Model;

use Rbnb\System\Database\Model;
use Rbnb\Database\Repository\RepositoryManager;
use Rbnb\Database\Repository\UserRepository;
use Rbnb\Database\Repository\CredentialsRepository; 
use Rbnb\Database\Repository\ProfileRepository; 

class User extends Model {
    public $email;
    public $password;
    public $banned;
    public $role_id;
    public $credentials_id;
    public $profile_id;

    protected $role = null;
    protected $credential = null;
    protected $profile = null;
    protected $reservation = null;
    protected $ratings = null;

    public function getRole(): ?Role {
        if(!$this->role) { 
            $this->role = RepositoryManager::instance()->getRepository('role')->getById((int)$this->role_id);
        }
        return $this->role;
    }

    public function getCredential(): ?Credential {
        if(!$this->credential) { 
            $this->credential = RepositoryManager::instance()->getRepository('credential')->getById((int)$this->credentials_id);
        }
        return $this->credential;
    }

    public function getProfile(): ?Profile {
        if(!$this->profile) { 
            $this->profile = RepositoryManager::instance()->getRepository('profile')->getById((int)$this->profile_id);
        }
        return $this->profile;
    }

    public function getRatings(): array {
        if(is_null($this->ratings)) {
            $this->ratings = RepositoryManager::instance()->getRepository('rating')->getAllByUser((int)$this->id, true);
        }
        return $this->ratings;
    }

    public function getReservations(): array {
        if(is_null($this->reservation)) {
            $this->reservation = RepositoryManager::instance()->getRepository('reservation')->getAllByUser((int)$this->id, true);
        }
        return $this->reservation;
    }

    public function toArray(): array {
        return $this->toFieldsArray([
            'email',
            'password',
            'banned',
            'role_id',
            'credential_id',
            'profile_id'
        ]);
    }
}