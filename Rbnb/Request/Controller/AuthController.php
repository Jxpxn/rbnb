<?php

namespace Rbnb\Request\Controller;

use Rbnb\System\Request\Controller;
use Rbnb\Rbnb;
use Rbnb\Settings;

use Rbnb\System\Authentification\Auth;

use Rbnb\System\Session\Session;
use Rbnb\System\Session\Session_Dictionnary;
use Rbnb\System\Session\SessionUser;
use Rbnb\System\Session\Form\SessionInputs;

use Rbnb\Database\Repository\RepositoryManager;
use Rbnb\Database\Repository\UserRepository;
use Rbnb\Database\Model\User;
use Rbnb\Database\Model\Role;

use Zend\Diactoros\Response\RedirectResponse;
use Zend\Diactoros\ServerRequest;


class AuthController extends Controller {
    public static function getRoleDefaultRouteName($role): string {
        $output = 'user_home';
        return $output;
    }

    public function loginPage(): void {
        $session_inputs = Session::get(Session_Dictionnary::FORM_SESSION);
        $session_inputs = !is_null($session_inputs) && $session_inputs instanceof SessionInputs ? $session_inputs : new SessionInputs();
        Session::destroy();
        echo $this->twig->render('visitor/login.twig', [
            'form_status' => $session_inputs
        ]);
    }

    public function registerPage(): void {
        $session_inputs = Session::get(Session_Dictionnary::FORM_SESSION);
        $session_inputs = !is_null($session_inputs) && $session_inputs instanceof SessionInputs ? $session_inputs : new SessionInputs();
        Session::destroy();
        echo $this->twig->render('visitor/register.twig', [
            'form_status' => $session_inputs
        ]);
    }

    public function auth( ServerRequest $request ) {
        $data = $request->getParsedBody();
        $router = Rbnb::instance()->getRouter();

        $email_field = 'email';
        $password_field = 'password';

        $email = !empty($data[$email_field]) ? $data[$email_field] : null; 
        $password = !empty($data[$password_field]) ? $data[$password_field] : null; 

        $session_inputs = new SessionInputs();

        if(is_null($email)) { 
            SessionInputs::constructError($session_inputs, SessionInputs::EMPTY_EMAIL); 
        }
        else {
            $session_inputs->setValue('email', $email);
        }
        if(is_null($password)) { 
            SessionInputs::constructError($session_inputs, SessionInputs::EMPTY_PASSWORD);
        }
        else {
            $session_inputs->setValue('password', $password);
        }

        if($session_inputs->hasError('email') || $session_inputs->hasError('password')) {
            Session::set(Session_Dictionnary::FORM_SESSION, $session_inputs);
            return new RedirectResponse( $router->url('login') );
        }

        $auth = Rbnb::instance()->getAuth();

        $user_repo = RepositoryManager::instance()->getRepository('user');
        $user = $user_repo->getByEmail($email);

        if(!$user || $auth->hashPassword($password) != $user->password) {
            SessionInputs::constructError($session_inputs, SessionInputs::INVALIDE_CREDENTIALS); 
        }

        if($session_inputs->hasError('form')) {
            $session_inputs->setValue('password', '');
            Session::set(Session_Dictionnary::FORM_SESSION, $session_inputs);
            return new RedirectResponse( $router->url('login') );
        }

        Session::set(Session_Dictionnary::USER, new SessionUser($user->email, $auth->hashSessionPassword($password)) );

        return new RedirectResponse( $router->url( self::getRoleDefaultRouteName($user->role_id) ));
    }

    public function authRegister( ServerRequest $request ) {
        $data = $request->getParsedBody();
        $router = Rbnb::instance()->getRouter();

        $session_inputs = new SessionInputs();

        $is_correct = true;
        $role = !empty($data['announcer']) ? Role::ID_ANNOUNCER : Role::ID_USER;
        $fields_values = [];
        $fields = [
            'email',
            'password',
            'prenom',
            'nom',
            'birthdate',
            'country'
        ];

        if($role != Role::ID_USER) { 
            $session_inputs->setValue('announcer', 'checked');
         }

        foreach($fields as $field) {
            $value = !empty($data[$field]) ? $data[$field] : null;
            if(is_null($value)) {
                $is_correct = false;
                $fields_values[$field] = null;
                SessionInputs::constructError($session_inputs, SessionInputs::EMPTY_FIELD, $field);
            }
            else {
                $fields_values[$field] = $value;
                $session_inputs->setValue($field, $value);
            }
        }

        if(!$is_correct) {
            $session_inputs->setValue('password', '');
            Session::set(Session_Dictionnary::FORM_SESSION, $session_inputs);
            return new RedirectResponse( $router->url('register') );
        }

        $auth = Rbnb::instance()->getAuth();

        $user_repo = RepositoryManager::instance()->getRepository('user');
        $user = $user_repo->getByEmail($fields_values['email']);

        if($user) { 
            $session_inputs->setValue('password', '');
            SessionInputs::constructError($session_inputs, SessionInputs::EMAIL_EXIST);
            Session::set(Session_Dictionnary::FORM_SESSION, $session_inputs);
            return new RedirectResponse( $router->url('register') );
        }

        // Insertion
        $settings = Settings::instance();
        #region Profile
        $profile_repo = RepositoryManager::instance()->getRepository('profile');
        $profile_id = $profile_repo->registerProfile($settings->get('default_avatar'), $settings->get('default_bio'));
        #endregion

        #region Credentials
        $credential_repo = RepositoryManager::instance()->getRepository('credential');
        $credential_id = $credential_repo->registerCredential(
            $fields_values['prenom'],
            $fields_values['nom'],
            $fields_values['birthdate'],
            $fields_values['country']
        );
        #endregion

        #region User
        $registered_user = $user_repo->registerUser(
            $fields_values['email'],
            $auth->hashPassword($fields_values['password']),
            $role,
            $credential_id,
            $profile_id
        );
        #endregion

        if($registered_user == -1) {
            //Remove added lines
            $credential_repo->removeCredential($credential_id);
            $profile_repo->removeProfile($profile_id);

            $session_inputs->setValue('password', '');
            SessionInputs::constructError($session_inputs, SessionInputs::OTHER_ERROR);
            Session::set(Session_Dictionnary::FORM_SESSION, $session_inputs);
            return new RedirectResponse( $router->url('register') );
        }

        return new RedirectResponse( $router->url( 'login' ));
    }

    public function logout() {
        Session::destroy();
        return new RedirectResponse( Rbnb::instance()->getRouter()->url('home') );
    }
}