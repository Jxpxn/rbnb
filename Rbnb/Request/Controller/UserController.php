<?php

namespace Rbnb\Request\Controller;

use Rbnb\Database\Model\Announcer;
use Rbnb\Database\Model\Room;
use Rbnb\Database\Repository\RepositoryManager;
use Rbnb\System\Request\Controller;
use Rbnb\Rbnb;

use Rbnb\System\Authentification\Auth;
use Rbnb\Database\Model\User;
use Rbnb\Utils\ArrayUtils;
use Rbnb\Utils\DateUtils;

use Rbnb\System\Session\Session;
use Rbnb\System\Session\Session_Dictionnary;
use Rbnb\System\Session\Form\SessionInputs;
use Rbnb\System\Session\SessionUser;

use Zend\Diactoros\Response\RedirectResponse;
use Zend\Diactoros\ServerRequest;

use DateTime;

class UserController extends Controller {
    public function home(): void {
        $current_user = Rbnb::instance()->getAuth()->getCurrentSessionUser();
        echo $this->twig->render(
            'logged/home.twig',
            [
                'user' => $current_user,
                'user_profile' => $current_user->getProfile(),
                'user_credentials' => $current_user->getCredential(),
                'rooms' => RepositoryManager::instance()->getRepository('room')->getAll()
            ]
        );
    }

    public function reservations(): void {
        $current_user = Rbnb::instance()->getAuth()->getCurrentSessionUser();
        echo $this->twig->render(
            'logged/my-reservations.twig',
            [
                'user' => $current_user,
                'user_profile' => $current_user->getProfile(),
                'user_credentials' => $current_user->getCredential()
            ]
        );
    }

    public function searchList(): void {

    }

    public function search(): void {
        
    }

    public function profile($id): void {
        $current_user = Rbnb::instance()->getAuth()->getCurrentSessionUser();
        $current_user_array = [
            'user' => $current_user,
            'user_profile' => $current_user->getProfile(),
            'user_credentials' => $current_user->getCredential()
        ];

        if(is_numeric($id)) {
            $int_id = (int)$id;
            $is_own_profile = $int_id == $current_user->id;
            $user = $is_own_profile ? $current_user : RepositoryManager::instance()->getRepository('user')->getById($int_id);
            if($user) {
                $path = $user instanceof Announcer ? 'profile/announcer.twig' : 'profile/user.twig';
                echo $this->twig->render(
                    $path,
                    ArrayUtils::appendArray($current_user_array,
                        [
                            'is_own_profile' => $is_own_profile,
                            'target' => $user,
                            'target_profile' => $user->getProfile(),
                            'target_credentials' => $user->getCredential()
                        ],
                        true
                    )
                );
                return;
            }
        }

        self::renderError("Erreur 404 - Ce profile n'existe pas :/", $this->twig);
    }

    public static function renderError(string $error_msg, $twig = null): void {
        $current_user = Rbnb::instance()->getAuth()->getCurrentSessionUser();
        $current_user_array = [
            'user' => $current_user,
            'user_profile' => $current_user->getProfile(),
            'user_credentials' => $current_user->getCredential()
        ];

        if(is_null($twig)) {
            $twig = Rbnb::instance()->getTwig();
        }
        
        echo $twig->render(
            'logged/error.twig',
            ArrayUtils::appendArray($current_user_array,
                [
                    'error_msg' => $error_msg
                ],
                true
            )
        );
    }

    public function editProfilePage(): void {
        $current_user = Rbnb::instance()->getAuth()->getCurrentSessionUser();
        echo $this->twig->render(
            'profile/user_edit.twig',
            [
                'user' => $current_user,
                'user_profile' => $current_user->getProfile(),
                'user_credentials' => $current_user->getCredential()
            ]
        );
    }

    public function editProfile( ServerRequest $request ) {
        $data = $request->getParsedBody();
        $auth = Rbnb::instance()->getAuth();
        $router = Rbnb::instance()->getRouter();
        $current_user = $auth->getCurrentSessionUser();
        $current_user_profile = $current_user->getProfile();

        $fields = [
            'bio',
            'avatar_filename'
        ];

        foreach($fields as $field) {
            $value = !empty($data[$field]) ? $data[$field] : null;
            if(!is_null($value)) {
                $current_user_profile->$field = $value;
            }
        }

        $profile_repo = RepositoryManager::instance()->getRepository('profile');
        $profile_repo->updateProfile( $current_user_profile );

        return new RedirectResponse( '/profile/' . $current_user->id ); 
    }

    public function settings(): void {
        $current_user = Rbnb::instance()->getAuth()->getCurrentSessionUser();
        $current_user_profile = $current_user->getProfile();
        $current_user_credentials = $current_user->getCredential();

        $form_status = Session::get(Session_Dictionnary::FORM_SESSION);
        if(is_null($form_status) || !($form_status instanceof SessionInputs)) {
            $form_status = new SessionInputs();
            $form_status->setValue('email', $current_user->email);
            $form_status->setValue('first_name', $current_user_credentials->first_name);
            $form_status->setValue('last_name', $current_user_credentials->last_name);
            $form_status->setValue('country', $current_user_credentials->country);
            $form_status->setValue('birth_date', $current_user_credentials->birth_date);
        }
        Session::set(Session_Dictionnary::FORM_SESSION, null);
        echo $this->twig->render(
            'logged/settings.twig',
            [
                'form_status' => $form_status,
                'user' => $current_user,
                'user_profile' => $current_user_profile,
                'user_credentials' => $current_user_credentials
            ]
        );
    }

    public function editSettings( ServerRequest $request ) {
        $data = $request->getParsedBody();
        $auth = Rbnb::instance()->getAuth();
        $router = Rbnb::instance()->getRouter();
        $current_user = $auth->getCurrentSessionUser();
        $current_user_credentials = $current_user->getCredential();

        $form_status = new SessionInputs();
        $is_correct = true;
        $fields_values = [];
        $fields = [
            'email',
            'birth_date',
            'first_name',
            'last_name',
            'country'
        ];

        foreach($fields as $field) {
            $value = !empty($data[$field]) ? $data[$field] : null;
            if(is_null($value)) {
                $is_correct = false;
                SessionInputs::constructError($form_status, SessionInputs::EMPTY_FIELD, $field);
            }
            else {
                $form_status->setValue($field, $value);
                $fields_values[$field] = $value;
            }
        }

        if($is_correct) {
            $user_repo = RepositoryManager::instance()->getRepository('user');
            $credential_repo = RepositoryManager::instance()->getRepository('credential');
            #region User
            $change_user = false;
            #region Email
            $target_email = $fields_values['email'];
            if($target_email != $current_user->email) {
                if(!is_null($user_repo->getByEmail($target_email))) {
                    SessionInputs::constructError($form_status, SessionInputs::EMAIL_EXIST);
                    Session::set(Session_Dictionnary::FORM_SESSION, $form_status);
                    return new RedirectResponse( $router->url('user_settings') );
                }
                $change_user |= true;
                $current_user->email = $target_email;
            }
            #endregion

            #region Password
            $old_password = !empty($data['o_password']) ? $data['o_password'] : null;
            $new_password = !empty($data['n_password']) ? $data['n_password'] : null;
            
            $change_password = !is_null($old_password) && !is_null($new_password);
            if($change_password) {
                if($auth->hashPassword($old_password) != $current_user->password) {
                    $form_status->setError('o_password', 'Mot de passe invalide');
                    Session::set(Session_Dictionnary::FORM_SESSION, $form_status);
                    return new RedirectResponse( $router->url('user_settings') );
                }
                $hashed_password = $auth->hashPassword($new_password);
                if($hashed_password != $current_user->password) {
                    $change_user |= true;
                    $current_user->password = $hashed_password;
                }
            }
            #endregion
            #endregion

            #region Credentials
            $change_credentials = false;
            foreach($fields_values as $key => $value) {
                if(\property_exists($current_user_credentials, $key) && $current_user_credentials->$key != $value) {
                    $change_credentials |= true;
                    $current_user_credentials->$key = $value;
                }
            }
            #endregion

            if($change_credentials) {
                $credential_repo->updateCredential($current_user_credentials);
            }
            if($change_user) {
                Session::set(Session_Dictionnary::USER, 
                    new SessionUser(
                        $current_user->email, 
                        $auth->hashSessionPassword($new_password)
                    )
                );
                $user_repo->updateUser($current_user);
            }

            return new RedirectResponse( $router->url('user_home') );
        }

        Session::set(Session_Dictionnary::FORM_SESSION, $form_status);

        return new RedirectResponse( $router->url('user_settings') );
    }

    public function showRoom($id): void {
        if(!\is_numeric($id)) {
            self::renderError("Erreur 404 - Cette chambre n'existe pas :/", $this->twig);
            return;
        }

        $auth = Rbnb::instance()->getAuth();
        $router = Rbnb::instance()->getRouter();
        $current_user = $auth->getCurrentSessionUser();
        $target_room = RepositoryManager::instance()->getRepository('room')->getById((int)$id);

        if(is_null($target_room)) {
            self::renderError("Erreur 404 - Cette chambre n'existe pas :/", $this->twig);
            return;
        }

        $form_status = Session::get(Session_Dictionnary::FORM_SESSION) ?? new SessionInputs();

        Session::set(Session_Dictionnary::FORM_SESSION, null);

        $is_self = $target_room->owner_id == $current_user->id;

        echo $this->twig->render(
            'room/room.twig',
            [
                'form_status' => $form_status,
                'user' => $current_user,
                'target_room' => $target_room,
                'is_self' => $is_self,
                'can_comment' => !$is_self && self::hadVisitedRoom($target_room, $current_user)
            ]
        );
    }

    public static function hadVisitedRoom( Room $room, User $user ) {
        $output = false;

        $reservations = $room->getReservations();
        if(count($reservations) > 0) {
            foreach($reservations as $reservation) {
                if($reservation->user_id == $user->id) {
                    $output = true;
                    break;
                }
            }
        }

        return $output;
    }

    public function postComment( ServerRequest $request, $id ) {
        if(!\is_numeric($id)) {
            self::renderError("Erreur 404 - Cette chambre n'existe pas :/", $this->twig);
            return;
        }

        $data = $request->getParsedBody();
        $auth = Rbnb::instance()->getAuth();
        $router = Rbnb::instance()->getRouter();
        $current_user = $auth->getCurrentSessionUser();

        $target_room = RepositoryManager::instance()->getRepository('room')->getById((int)$id);

        if(is_null($target_room)) {
            self::renderError("Erreur 404 - Cette chambre n'existe pas :/", $this->twig);
            return;
        }

        $form_status = new SessionInputs();

        $is_correct = true;
        $fields_values = [];
        $fields = [
            'rate',
            'comment'
        ];

        foreach($fields as $field) {
            $value = !empty($data[$field]) ? $data[$field] : null;
            if(is_null($value)) {
                $is_correct = false;
                SessionInputs::constructError($form_status, SessionInputs::EMPTY_FIELD, $field);
            }
            else {
                $form_status->setValue($field, $field);
                $fields_values[$field] = $value;
            }
        }

        if($is_correct) {
            $rating_repo = RepositoryManager::instance()->getRepository('rating');
            $rating = $rating_repo->registerRating(
                (int)$id,
                (int)$current_user->id,
                (int)$fields_values['rate'],
                $fields_values['comment']
            );
        }
        else {
            Session::set(Session_Dictionnary::FORM_SESSION, $form_status);
        }

        return new RedirectResponse( $router->url('user_room_detail', [ 'id' => $id ]) );
    }

    public function reserver( ServerRequest $request, $id ) {
        if(!\is_numeric($id)) {
            self::renderError("Erreur 404 - Cette chambre n'existe pas :/", $this->twig);
            return;
        }

        $data = $request->getParsedBody();
        $auth = Rbnb::instance()->getAuth();
        $router = Rbnb::instance()->getRouter();
        $current_user = $auth->getCurrentSessionUser();
        $reservation_repo = RepositoryManager::instance()->getRepository('reservation');

        $target_room = RepositoryManager::instance()->getRepository('room')->getById((int)$id);

        if(is_null($target_room)) {
            self::renderError("Erreur 404 - Cette chambre n'existe pas :/", $this->twig);
            return;
        }

        if($target_room->owner_id == $current_user->id) {
            self::renderError("Error - You don't have the permission", $this->twig);
            return;
        }

        $form_status = new SessionInputs();

        $is_correct = true;
        $fields_values = [];
        $fields = [
            'start_time',
            'end_time'
        ];

        foreach($fields as $field) {
            $value = !empty($data[$field]) ? $data[$field] : null;
            if(is_null($value)) {
                $is_correct = false;
                SessionInputs::constructError($form_status, SessionInputs::EMPTY_FIELD, $field);
            }
            else {
                $form_status->setValue($field, $field);
                $fields_values[$field] = $value;
            }
        }

        if($is_correct) {
            #region Date Check
            $start_time = new DateTime( $fields_values['start_time'] );
            $end_time = new DateTime( $fields_values['end_time'] );

            if($start_time > $end_time || $start_time < new DateTime()) {
                $is_correct = false;
                SessionInputs::constructError($form_status, SessionInputs::INVALIDE_FIELD, 'start_time');
                SessionInputs::constructError($form_status, SessionInputs::INVALIDE_FIELD, 'end_time');
            } 

            #region Collision check
            if($is_correct) {
                $hi = $reservation_repo->getAllByRoom((int)$id);
                if(count($hi) > 0) {
                    foreach($hi as $res) {
                        $_start_time = new DateTime($res->start_time);
                        $_end_time = new DateTime($res->end_time);
                        
                        if(DateUtils::dateIsInRange($_start_time, $_end_time, $start_time)) {
                            $is_correct = false;
                            SessionInputs::constructError($form_status, SessionInputs::INVALIDE_FIELD, 'start_time');
                        }
                        if(DateUtils::dateIsInRange($_start_time, $_end_time, $end_time)) {
                            $is_correct = false;
                            SessionInputs::constructError($form_status, SessionInputs::INVALIDE_FIELD, 'end_time');
                        }
                        if(!$is_correct) { break; }
                    }
                }
            }
            #endregion

            #endregion

            if($is_correct) {
                $inserted = $reservation_repo->registerReservation(
                    (int)$id,
                    (int)$current_user->id,
                    $fields_values['start_time'],
                    $fields_values['end_time']
                );

                return new RedirectResponse( $router->url('user_reservations') );
            }
        }

        Session::set(Session_Dictionnary::FORM_SESSION, $form_status);

        return new RedirectResponse( $router->url('user_room_detail', [ 'id' => $id ]) );
    }

    public function cancelReservation( $id ) {
        if(!\is_numeric($id)) {
            self::renderError("Erreur 404 - Cette reservation n'existe pas :/", $this->twig);
            return;
        }

        $auth = Rbnb::instance()->getAuth();
        $router = Rbnb::instance()->getRouter();
        $current_user = $auth->getCurrentSessionUser();
        $target_res = RepositoryManager::instance()->getRepository('reservation')->getById((int)$id);

        if(is_null($target_res)) {
            self::renderError("Erreur 404 - Cette reservation n'existe pas :/", $this->twig);
            return;
        }

        $is_room_owner = false;

        if($target_res->user_id != $current_user->id) {
            if($current_user->id == $target_res->getRoom()->owner_id) {
                $is_room_owner = true;
            }
            else {
                self::renderError("Error - you don't have the permissions :/", $this->twig);
                return;
            }
        }

        RepositoryManager::instance()->getRepository('reservation')->removeReservation((int)$id);

        if($is_room_owner) {
            return new RedirectResponse( $router->url('announcer_rooms_reservation', [ 'id' => $target_res->room_id]) );
        }

        return new RedirectResponse( $router->url('user_reservations') );
    }

    public function reservationPage(): void {

    }
}