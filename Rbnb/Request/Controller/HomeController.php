<?php

namespace Rbnb\Request\Controller;

use Rbnb\System\Request\Controller;

class HomeController extends Controller {
    public function index(): void {
        echo $this->twig->render('visitor/home.twig', ['title' => 'Hello world']);
    }
}