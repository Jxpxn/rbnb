<?php

namespace Rbnb\Request\Controller;

use Rbnb\System\Request\Controller;
use Rbnb\Rbnb;
use Rbnb\System\Authenfication\Auth;
use Rbnb\Database\Model\User;

use Rbnb\System\Session\Session;
use Rbnb\System\Session\Session_Dictionnary;
use Rbnb\System\Session\SessionUser;
use Rbnb\System\Session\Form\SessionInputs;

use Rbnb\Database\Repository\RepositoryManager;

use Rbnb\Utils\ArrayUtils;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Diactoros\ServerRequest;

class AnnouncerController extends Controller {
    public function searchList(): void {
        echo $this->twig->render('announcer/search-list.twig', ['title' => 'Hello world']);
    }

    public function search(): void {
        
    }

    public function dashboard(): void {
        $current_user = Rbnb::instance()->getAuth()->getCurrentSessionUser();
        $user_rooms = $current_user->getRooms();
        echo $this->twig->render(
            'announcer/dashboard.twig', 
            [
                'user' => $current_user,
                'user_profile' => $current_user->getProfile(),
                'user_credentials' => $current_user->getCredential(),
                'user_rooms' => $user_rooms,
                'user_rooms_count' => count($user_rooms)
            ]
        );
    }

    public function addRoomPage(): void {
        $current_user = Rbnb::instance()->getAuth()->getCurrentSessionUser();
        $form_status = Session::get(Session_Dictionnary::FORM_SESSION);
        if(is_null($form_status)) {
            $form_status = new SessionInputs();
            $form_status->setValue('price', '1');
            $form_status->setValue('size', '1');
            $form_status->setValue('slots', '1');
        }

        Session::set(Session_Dictionnary::FORM_SESSION, null);
        echo $this->twig->render(
            'announcer/room_adder.twig', 
            [
                'form_status' => $form_status,
                'user' => $current_user,
                'equipements' => RepositoryManager::instance()->getRepository('equipement')->getAll(),
                'types' => RepositoryManager::instance()->getRepository('room_type')->getAll()
            ]
        );
    }

    public function registerRoom( ServerRequest $request ) {
        $data = $request->getParsedBody();
        $auth = Rbnb::instance()->getAuth();
        $router = Rbnb::instance()->getRouter();
        $current_user = $auth->getCurrentSessionUser();

        $form_status = new SessionInputs();

        $is_correct = true;
        $fields_values = [];
        $fields = [
            'title',
            'type_id',
            'price',
            'size',
            'description',
            'slots',
            'city',
            'country'
        ];

        $equipements = !empty($data['equipements']) ? $data['equipements'] : [];

        $form_status->setArray('equipements', $equipements);

        foreach($fields as $field) {
            $value = !empty($data[$field]) ? $data[$field] : null;
            if(is_null($value)) {
                $is_correct = false;
                SessionInputs::constructError($form_status, SessionInputs::EMPTY_FIELD, $field);
            }
            else {
                $form_status->setValue($field, $value);
                $fields_values[$field] = $value;
            }
        }

        if($is_correct) {
            Session::set(Session_Dictionnary::FORM_SESSION, null);

            $room_repo = RepositoryManager::instance()->getRepository('room');
            $room_id = $room_repo->registerRoom(
                $fields_values['title'],
                $fields_values['type_id'],
                $fields_values['price'],
                $fields_values['size'],
                $fields_values['description'],
                $current_user->id,
                $fields_values['slots'],
                $fields_values['city'],
                $fields_values['country']
            );

            if($room_id == -1) {
                UserController::renderError('Erreur lors de la creation de la chambre :/');
                return;
            }

            if(count($equipements) > 0) {
                $equipement_repo = RepositoryManager::instance()->getRepository('equipement');
                $room_equipement_repo = RepositoryManager::instance()->getRepository('room_equipement');
                foreach($equipements as $equipement) {
                    if($equipement_repo->idExist($equipement)) {
                        $room_equipement_repo->registerRoomEquipement($equipement, $room_id);
                    }
                }
            }

            return new RedirectResponse( $router->url('announcer_dashboard') );
        }

        Session::set(Session_Dictionnary::FORM_SESSION, $form_status);

        return new RedirectResponse( $router->url('announcer_add_room') );
    }

    public function removeRoom($id) {
        if(is_numeric($id)) {
            $router = Rbnb::instance()->getRouter();
            $auth = Rbnb::instance()->getAuth();
            $current_user = $auth->getCurrentSessionUser();
    
            $room_repo = RepositoryManager::instance()->getRepository('room');
    
            $room = $room_repo->getById((int)$id);
    
            if($room && $room->owner_id == $current_user->id) {
                $room_equipement_repo = RepositoryManager::instance()->getRepository('room_equipement');
                $reservation_repo = RepositoryManager::instance()->getRepository('reservation');
                $rating_repo = RepositoryManager::instance()->getRepository('rating');
                $rating_repo->removeByRoom((int)$id);
                $reservation_repo->removeByRoom((int)$id);
                $room_equipement_repo->removeRoomEquipementbyRoom($room->id);
                $room_repo->removeRoom($room->id);
            }
    
            return new RedirectResponse( $router->url('announcer_dashboard') );
        }

        UserController::renderError("Erreur 404 - Cette chambre n\'existe pas :/");
        return;
    }

    public function editRoomPage($id): void {
        if(!is_numeric($id)) {
            UserController::renderError("Erreur 404 - Cette chambre n\'existe pas :/");
            return;
        }

        $auth = Rbnb::instance()->getAuth();
        $router = Rbnb::instance()->getRouter();
        $current_user = $auth->getCurrentSessionUser();
        $target_room = RepositoryManager::instance()->getRepository('room')->getById((int)$id);

        if(is_null($target_room)) {
            UserController::renderError("Erreur 404 - Cette chambre n\'existe pas :/");
            return;
        }

        $form_status = Session::get(Session_Dictionnary::FORM_SESSION);
        if(is_null($form_status)) {
            $form_status = new SessionInputs();
            foreach($target_room->toArray() as $key => $value) {
                $form_status->setValue($key, $value);
            }

            #region Equipements
            $target_room_equipements = [];
            foreach($target_room->getEquipements() as $room_equipement) {
                array_push($target_room_equipements, $room_equipement->equipement_id);
            }
            $form_status->setArray('equipements', $target_room_equipements);
            #endregion
        }

        Session::set(Session_Dictionnary::FORM_SESSION, null);

        echo $this->twig->render(
            'announcer/room_editor.twig',
            [
                'form_status' => $form_status,
                'user' => $current_user,
                'equipements' => RepositoryManager::instance()->getRepository('equipement')->getAll(),
                'types' => RepositoryManager::instance()->getRepository('room_type')->getAll(),
                'target_room' => $target_room
            ]
        );
    }

    public function editRoom( ServerRequest $request, $id ) {
        if(!is_numeric($id)) {
            UserController::renderError("Erreur 404 - Cette chambre n\'existe pas :/");
            return;
        }

        $data = $request->getParsedBody();
        $auth = Rbnb::instance()->getAuth();
        $router = Rbnb::instance()->getRouter();
        $current_user = $auth->getCurrentSessionUser();
        $target_room = RepositoryManager::instance()->getRepository('room')->getById((int)$id);

        if(is_null($target_room)) {
            UserController::renderError("Erreur 404 - Cette chambre n\'existe pas :/");
            return;
        }

        if($target_room->owner_id != $current_user->id) {
            UserController::renderError("Erreur - Vous n'avez pas la permission");
            return;
        }

        $form_status = new SessionInputs();

        $is_correct = true;
        $fields_values = [];
        $fields = [
            'title',
            'type_id',
            'price',
            'size',
            'description',
            'slots'
        ];

        $equipements = !empty($data['equipements']) ? $data['equipements'] : [];

        $form_status->setArray('equipements', $equipements);

        foreach($fields as $field) {
            $value = !empty($data[$field]) ? $data[$field] : null;
            if(is_null($value)) {
                $is_correct = false;
                SessionInputs::constructError($form_status, SessionInputs::EMPTY_FIELD, $field);
            }
            else {
                $form_status->setValue($field, $value);
                $fields_values[$field] = $value;
                $target_room->$field = $value;
            }
        }

        if($is_correct) {
            Session::set(Session_Dictionnary::FORM_SESSION, null);

            $target_room_equipements = [];

            foreach($target_room->getEquipements() as $room_equipement) {
                array_push($target_room_equipements, $room_equipement->equipement_id);
            }

            $room_repo = RepositoryManager::instance()->getRepository('room');
            $room_repo->updateRoom( $target_room );


            if(!ArrayUtils::arrayHasSameValues($equipements, $target_room_equipements)) {
                $equipement_repo = RepositoryManager::instance()->getRepository('equipement');
                $room_equipement_repo = RepositoryManager::instance()->getRepository('room_equipement');

                $room_equipement_repo->removeRoomEquipementbyRoom($target_room->id);

                foreach($equipements as $equipement) {
                    if($equipement_repo->idExist($equipement)) {
                        $room_equipement_repo->registerRoomEquipement($equipement, $target_room->id);
                    }
                }
            }

            return new RedirectResponse( $router->url('announcer_dashboard') );
        }

        Session::set(Session_Dictionnary::FORM_SESSION, $form_status);

        return new RedirectResponse( $router->url('announcer_add_room') );
    }

    public function roomReservation($id): void {
        if(!is_numeric($id)) {
            UserController::renderError("Erreur 404 - Cette chambre n\'existe pas :/");
            return;
        }
        $auth = Rbnb::instance()->getAuth();
        $router = Rbnb::instance()->getRouter();
        $current_user = $auth->getCurrentSessionUser();
        $target_room = RepositoryManager::instance()->getRepository('room')->getById((int)$id);

        if(is_null($target_room)) {
            UserController::renderError("Erreur 404 - Cette chambre n\'existe pas :/");
            return;
        }

        if($target_room->owner_id != $current_user->id) {
            UserController::renderError("Erreur - Vous n'avez pas la permission");
            return;
        }

        echo $this->twig->render(
            'announcer/reservations.twig',
            [
                'user' => $current_user,
                'target' => $target_room
            ]
        );
    }
}