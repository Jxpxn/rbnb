<?php

namespace Rbnb\Request;

use Closure;
use MiladRahimi\PhpRouter\Router;

abstract class Routes {
    public static function visitor(): Closure {
        return function( Router $router ) {
            $router
                ->name('home')
                ->get('/', 'HomeController@index')

                ->name('login')
                ->get('/login', 'AuthController@loginPage')

                ->name('register')
                ->get('/register', 'AuthController@registerPage')

                ->name('auth')
                ->post('/auth', 'AuthController@auth')

                ->name('authregister')
                ->post('/authregister', 'AuthController@authRegister')
            ;
        };
    }

    public static function user(): Closure {
        return function( Router $router ) {
            $router
                ->name('logout')
                ->get('/logout', 'AuthController@logout')

                ->name('user_home')
                ->get('/home', 'UserController@home')

                ->name('user_reservations')
                ->get('/reservations', 'UserController@reservations')

                ->name('user_profile')
                ->get('/profile/{id}', 'UserController@profile')

                ->name('user_settings')
                ->get('/settings', 'UserController@settings')

                ->name('user_settings_auth')
                ->post('/settings_confirm', 'UserController@editSettings')

                ->name('user_profile_edit')
                ->get('/edit_profile', 'UserController@editProfilePage')

                ->name('user_profile_edit_auth')
                ->post('/edit_profile_confirm', 'UserController@editProfile')

                ->name('user_room_detail')
                ->get('/room/{id}', 'UserController@showRoom')

                ->name('user_room_postcomment')
                ->post('/post_comment/{id}', 'UserController@postComment')

                ->name('user_room_reserver')
                ->post('/reserver/{id}', 'UserController@reserver')

                ->name('user_room_cancel_reservation')
                ->get('/cancel_reservation/{id}', 'UserController@cancelReservation')
            ;
        };
    }

    public static function announcer(): Closure {
        return function( Router $router ) {
            $router
                ->name('announcer_dashboard')
                ->get('/dashboard', 'AnnouncerController@dashboard')

                ->name('announcer_add_room')
                ->get('/add_room', 'AnnouncerController@addRoomPage')

                ->name('announcer_register_room')
                ->post('/register_room', 'AnnouncerController@registerRoom')

                ->name('announcer_edit_room')
                ->get('/edit_room/{id}', 'AnnouncerController@editRoomPage')

                ->name('announcer_update_room')
                ->post('/update_room/{id}', 'AnnouncerController@editRoom')

                ->name('announcer_remove_room')
                ->get('/remove_room/{id}', 'AnnouncerController@removeRoom')

                ->name('announcer_rooms_reservation')
                ->get('/room_reservation/{id}', 'AnnouncerController@roomReservation')
            ;
        };
    }
}