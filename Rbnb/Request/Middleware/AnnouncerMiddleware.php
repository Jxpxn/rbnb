<?php

namespace Rbnb\Request\Middleware;

use Closure;

use Rbnb\Rbnb;
use Rbnb\System\Session\Session;
use Rbnb\System\Session\Session_Dictionnary;
use Rbnb\System\Session\SessionUser;

use Rbnb\Database\Model\Role;

use Rbnb\Request\Controller\AuthController;

use MiladRahimi\PhpRouter\Middleware;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\RedirectResponse;

class AnnouncerMiddleware implements Middleware {
    public function handle( ServerRequestInterface $request, Closure $next )
	{
		$router = Rbnb::instance()->getRouter();
		$auth = Rbnb::instance()->getAuth();

        $session_user = $auth->checkIfSessionIsCorrect( Session::get( Session_Dictionnary::USER ) );

		if( $session_user['valide'] ) {
            if($session_user['user']->role_id == Role::ID_ANNOUNCER) {
                return $next( $request );	
            }
            return new RedirectResponse( $router->url( AuthController::getRoleDefaultRouteName($session_user['user']->role_id) ) );
        }

        Session::destroy();

		return new RedirectResponse( $router->url( 'login' ) );
	}
}